# README # I love BIT 301

This repo is where you will get and store your required markdown format of the BIT301 files.

##What to do##

**Fork** this repository
***IMPORTANT NOTE*** For Owner, make sure you use your team name.
![GitFork.jpg](https://bitbucket.org/repo/eXxGbX/images/2742344142-GitFork.jpg)

To fork, click on the Fork icon on the left side of the screen:
![chooseFork.jpg](https://bitbucket.org/repo/eXxGbX/images/479019916-chooseFork.jpg)

Please use Markdown format so that you can view the files easily in Bitbucket. 
[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
Use a [Markdown editor](https://www.google.com/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=markdown%20editor%20free) to edit the files, or do it online.

You will need to use Git to manage your files in your team's repository. 
Refer to the Git Commands file or the excellent Git tutorials [here](https://www.atlassian.com/git/tutorials)

##For more information about the assignment please refer to the [Wiki](https://bitbucket.org/ngsm/bit301/wiki/Home)##
