##Work Breakdown Structure for (Project Name)

Prepared by: Ong Ming Yee & Kuah Peng He			Date: 1/10/2015


**Initiating** 
1.	Team meeting/ discussion

 1.1 Identify venue for meeting/ discussion
    
 1.2 Identify date and time for meeting
    
 1.3 Learning BitBucket
    
 1.4 Learning Git and bash

2.	Analyse Three Sphere Model and Stakeholders

 2.1 Stakeholders Analysis
    
 2.2 Research issues (Business, Technology, Organization)
    
 2.3 Three Sphere Model and Stakeholders Completed

3.	Understanding the Business Case

 3.1 Develop the Business Case
    
>   3.1.1	Knowing the Background of the Project 
        
>   3.1.2	Planning objectives
        
>   3.1.3	Understanding situations/ problems and opportunities
        
>   3.1.4	Analyse option and recommendation
        
>   3.1.5	Research preliminary requirements of the project
        
>   3.1.6	Estimate budget and analyse financial data
        
>   3.1.7	Risks of the project
    
        3.2 Business Case Completed

4.	Project charter
    
 4.1 Assign Project Charter
    
> 4.1.1	Analyse project topic

> 4.1.2	Planning start date and finish date

> 4.1.3	Research project objectives

> 4.1.4	Analyse project success criteria

> 4.1.5	Assign roles and responsibilities

4.2	Project Charter Assigned

**Planning**

**Planning – A1**

5.Scope Statement 

5.1 Develop Scope Statement

> 5.1.1 Project justification

> 5.1.2 Product characteristics and requirements

> 5.1.3 Project deliverables

> 5.1.4 Project Success Criteria

5.2 Scope Statement Completed

6.Work Breakdown Structure 

 6.1 Create WBS

 6.2 WBS created

7.Work Item List 

 7.1 Create Work Item List

 7.2 Work Item List created

8.Baseline Gantt Chart

 8.1 Designing Gantt Chart 

 8.2 Gantt Chart completed

**Planning – A2**

9.Cost Estimate

 9.1 Develop Cost Estimate

 9.2 Cost Estimate Completed

10.Quality Management Plan

 10.1 Develop Quality Management Plan

 10.2 Quality Management Plan Completed

11.Risk Management Plan

 11.1 Develop Risk Management Plan

 11.2 Risk Management Plan Completed

**Executing** 

12.Research for the "History of Java" topic

13.Summary of the “History of Java” in BitBucket

14.Storyboard for Wiki Project

>   14.1 Designing the images

>   14.2 Shooting Video

>   14.3 Editing Video for add Subtitles and Effects

**Monitoring and Controlling**

**Monitoring & Controlling – A1**

15.Issue Tracking

16.Tracking Gantt chart

17.Progress report

**Monitoring & Controlling – A2**

18.Earned Value Analysis

19.Issue Tracking

20.Tracking Gantt Chart

21.Quality Control

22.Risk Management

**Closing**

23.Finalize the documentation - A1

24.Finalize Completed - A1

25.Finalize the documentation - A2

26.Prepare Final Project Presentation 

27.Final Project Completed - A2
