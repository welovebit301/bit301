**Business Case for History of Java Research**

**Date:** 1/10/2015

**Prepared by:** Ong Ming Yee, Kuah Peng He, Bavani a/p Malaiyappan

**1.0 Introduction/ Background**

Initially we are assigned to develop a wiki page about a specific topic which is History of Java. We are allowed to 
create and edit the content of our wiki page about the topic given. My group mates and I should spend more time 
and also take some effort on creating or developing our wiki with complete information about the History of Java. 
This information should be beneficial for current Java students. Thus, we are doing a fine research on Java to make sure our information is true. 

**2.0 Business Objective**

**3.0 Current Situation and Problem/Opportunity Statement**   
The reason why we wanted to do this project of developing a wiki is current Java students having lack of 
knowledge about their subject Java. This makes them to hate this particular subject without learning it 
properly. Almost every student is just doing this subject just to complete their study plan. But actually 
their interesting level to learn the subject is quite low. Usually these students just learn how to do the
coding of Java in their class and that is what teaches by the lecturers too. These are the reasons that 
make us to create a wiki for them about the History of Java. 

**4.0 Critical Assumption and Constraints**

The critical assumption of our project is nowadays the Java students not aware about their subject history. They 
also give less attention towards the subject because the level of interesting to learn this subject is very low. 
The current Java students are actually having lack knowledge about Java. Thus, to solve this problem we decided to 
develop a wiki for them. The time that needs for us to complete this wiki is just in 14 weeks. As we have a lack 
of time, we are hurrying our work to find out more about Java. Hopefully, we’ll complete this project within 
the estimated period.

**5.0 Analysis of Option and Recommendation**

#### There are few options for this project: ####

1. Doing nothing - The research on the history is not worth doing, and we should choose another topic and conduct a different research.
2. Doing another topic - Choosing another topic which is far more usefull and needed fro the users.
3. Doing the research according to the team’s knowledge and skills - Using the knowledge and skills and software available to work on the whole project.
4. Doing the research by following the instructions and learning from the lecturer and also doing it using all the skills and software available – Learn from the lecturer and apply the knowledge into the project, in addition the whole team work together and apply all the researches done into the project.  


**6.0 Preliminary Project Requirements**

**7.0 Budget Estimate and Financial Analysis**

**8.0 Schedule Estimate**

**9.0 Potential Risks**

1. The cost needed for the hardware and software used in the project might be insufficient.
2. The communication and participation of the internal and external organizations of the project might be inadequate.
3. The availability of the software and hardware needed and the skills to operate using them is lacking.
4. The commitment of the team that is responsible for the project is not strong and firm enough while doing the research.
5. The development of milestones and deliverables of the project might become inappropriate or imprecise.
6. The achievement of project requirements might become undesirable and unfit for the research topic.
7. Inappropriate data or design which leads to reimplementation, redo and redesign fro the project.

**10.0 Exhibits** Exhibit A: Financial Analysis (Insert image here)
