## Project Charter
Project Title: History of Java Research

Project Start Date: 25/8/2015

Projected Finish Date: 27/11/2015

Budget Information: RM 100,000

Project Manager: 
Name: Ong Ming Yee

E-mail: Patrickong1112@gmail.com

Project Objectives:
 
* To help students understand and appreciate the contributions of Java to the world.
* To help students understand the versions and improvement of Java throughout its years of development.
* To help students understand and analyse the Object-Oriented Modelling of Java.
* To help students understand more on cross-platform development/ adaptability of Java in different devices.

Main Project Success Criteria: 

The project would meet all the requirements of a project management plan, all instructions given by the Project Sponsor/ Lecturer are followed and utilized in the project furthermore and by all means is to complete the project within 14 weeks. 

Approach:

* Doing intensive and cognitive researches on the selected topic on various websites, journals and instructions from the Project Sponsor/ Lecturer.
* Finishing the project within the given time, work breakdown structure is created to help assign work to each member.
* Weekly meetings that discuss the progress of the project and all details are planned and recorded in the Gantt chart and Project Progress files.

#### Roles and Responsibilities ####

| Role           | Name                   | Organization    | Position | Email                         |
|----------------|------------------------|-----------------|----------|-------------------------------|
| Project Spnsor | Ng Shu Min             | HELP University | Lecturer | ngsm@help.edu.my              |
| Project Member | Kuah Peng He           | HELP University | Student  | Elven1116@gmail.com           |
| Project Member | Bavani A/P Malaiyappan | HELP University | Student  | Bavanimalaiyappan93@gmail.com |

Sign-off: (Signatures of all above stakeholders. Can sign by their names in table above.)

Comments: By following the instructions and step-by-step guide from the Project Sponsor, the team is able to get the suitable resources and be able to manage the project on accordingly and on time.
