##Work Items List for History of Java Research

| No | Work Item | Est Hours | Assigned to | Hours Worked | Status |
|----|-----------|-----------|-------------|--------------|--------|
| 1  | Introduction Background   | 3 hours      | Bavani             | 3.5 hours      |  Completed    |
| 2  | Business Objective       | 2 hours      | Ong Ming Yee     | 2 hours     | Completed    |
| 3  | Current Situation and Problem/ Opportunity Statement    | 4 hours    | Bavani    | 5 hours    | Completed   |
| 4  | Critical Assumption and Constraint     | 3 hours   | Bavani   | 3.5 hours    | Completed    |
| 5  | Analysis of Option and Recommendation    | 2 hour   | Ong Ming Yee    | 1.5 hours    | Completed    |
| 6  | Preliminary Project Requirements    | 3 hours   | Kuah Peng He    | 3 hours    | Completed    |
| 7  | Budget Estimate and Financial Analysis    | 5 hours    | Kuah Peng He    | 3.5 hours   | Completed    |
| 8  | Schedule Estimate    | 2 hours   | Ong Ming Yee    | 2 hours    | Completed    |
| 9  | Potential Risks     | 3 hours    | Ong Ming Yee     | 3 hours    | Completed   |
| 10 | Exhibits    | 6 hours    | Kuah Peng He    | 4.5 hours     | Completed    |
| 12 | Wiki Page   | 10 hours   | Ong Ming Yee    | 9 hours    | Completed    |
| 11 | Project Charter    | 6 hours    | Ong Ming Yee   | 6 hours   | Completed    | 
| 12 | Scope Statement   | 3 hours   | Bavani   | 4 hours   | Completed   |
| 13 | ThreeSphereModel   | 5 hours   | Ong Ming Yee & Kuah Peng He  | 5.5 hours    | Completed   |
| 14 | Issue Log    | 6 hours   | Ong Ming Yee    | 8 hours   | Completed    |
| 15 | Work Breakdown Structure   | 6 hours    | Kuah Peng He & Ong Ming Yee    | 6.5 hours    | Completed   |
| 16 | Gantt Chart   | 10 hours   | Kuah Peng He & Bavani    | 12 hours    | Completed    |
| 17 | Progress Report   | 6 hours   | Kuah Peng He    | 5 hours   | Completed    |