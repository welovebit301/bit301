####Schedule Estimation 

1.	The time estimations on the project tasks is the first for all projects’ schedule estimations, since there are limits for the end date to the project, the time to complete each tasks are estimated properly and this is the reason why Gantt Charts are created to track the time and task progress.

2.	The project sponsor/ lecturer would like to see the completion of the project within the time limit, there might be slack time for some tasks but the assignment that the team acquired had a long critical path for certain tasks.

3.	The project would also be assumed to have a useful life of at least 3 to 5 years for users to read and receive the information about the history of Java.

4.	The project would assumed that after the release of the project, students especially in the IT department would be intrigued and motivate to learn and appreciate the contributions of Java to them.

5.	The project manager is estimated to understand the team members’ weaknesses and strength and assign the proper tasks to specific teammates.

6.	Lastly an estimation of things going wrong would also be one, if the tasks are not completed within the scheduled time, the team has to come up with quick and fast solution such as reducing slack time for other tasks and compensate for the lost time.
