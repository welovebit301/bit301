﻿Scope Statement (Version 1)

Project Title: History of Java Research

Date: 23 Sept 2015

Prepared by: Bavani Malaiyappan 
________________________________________
Project Justification: We are doing this project to assist current Java students and lecturers. Our wiki will increase the level of interest of the students in their subject called Programming in Java. It will also help them to gain more knowledge about the History of Java. The images, videos and the information that contains on this wiki will provide a clear explanation for the students and also lecturers. It is very important to focus on updating our project after its completion as well.
________________________________________
Product Characteristics and Requirements:

1.	Links: All the links to the external sites appropriate to the project will be attached to the wiki. The reviewers can click and view the related links to get further information e.g. timeline of Java, producers and teams that created Java.
2.	Image: The image that produced by the team will be posted on the wiki to create a better and entertaining platform for the users. They will be attractive and informative showing users the information relates to the history of Java e.g. the producer, Java icons etc.
3.	Video: The video with audio and too subtitles will be attached to the wiki for the reviewers to gain additional knowledge. It will be informative and a summarized version for the written wiki, and it will show the evolution of Java and the deeds of Java.
________________________________________
Summary of Project Deliverables

Project management related deliverables: Business case, project charter, scope statement, work breakdown structure(WBS), progress reports, final project report, cost estimate, three sphere model and schedule estimate that required to manage this whole project.

Product related deliverables:

1. Survey: Conduct a small survey with the current Java students to help gather information and to conclude the content and structure of the wiki.
2. Articles: The wiki will include few beneficial articles relevant to Java history and store these articles in various formats such as URLs on the wiki.
3. Links: The wiki will contain links for the students to look for further information about the history of Java.
4. Wiki content: The wiki will include content for the templates section, articles section, images section, links section and videos section. The images will be designed using Adobe Photoshop CS6 software to create a unique image which will increase the attention of current Java students. After shoot the video, it will be edited using iMovie software which will be entertain  and beneficial for the students and other viewers.
5. Project benefit measurement plan: The project benefit plan will be the calculation of the financial cost of the wiki and also the requirments achieved in the end of the project.
________________________________________
Project Success Criteria: The team’s goal is to complete this whole project within 14 weeks for not more than RM100,000. The team is actually working hard on the project with full of confidence that it will be very helpful for everyone specifically current Java students.  If this project exceeds a little longer to complete and cost a little more than planned means it is still consider a successful project as it is more useful and helpful for the others. Most importantly is to follow the instructions and do the research according to the requirements of the lecturer,using all the software and hardware such as the Bitbucket and GanttProject for online wiki creation, discussions and Gantt creation.

