#Three Sphere Model Analysis for (History Of Java Research)#

**Team Members:**ONG MING YEE, KUAH PENG HE, BAVANI A/P MALAIYAPPAN

##Introduction##

Java is a programming language and computing platform first released in 1995 by the Sun Microsystems. It is fast, reliable and secure for most applications and websites when it is installed.  From game consoles, database centers to supercomputers and mobile phones and the internet, Java is supporting all these behind the scene. The project to do a research on the history of Java is to understand and appreciate the programming language that has been working behind all the devices that we hold in the palm of our hands every single day.

###Stakeholder Analysis###

####Internal to the organization####

* The students in the team – Show full respect to the teammates and making sure of all ideas are heard. On top of that there shall be no dominance upon teammates. In addition, all members are assigned with their own tasks and expected to be done on time, accurate and not plagiarized. 
* The lecturer – Team leaders are to be up to date and learn all necessary skills and knowledge from the lecturer to have an idea to start the project. In addition, letting the lecturer points out the mistakes made and make changes or correct the mistake that has been pointed out.

####External to the organization####

* IT department – The team members has to be on time during classes and meetings with the lecturer, venue and time are few of the most important things a team should have. 
* Web site references. – The web sites used during the research has to be sited properly. The information referred or used must not be plagiarized.

###Three Sphere Model###

Business

- What is the influence of the project?
- What are the project aims and objectives?
- Who will get the benefits by the end of the project?
- What are the financial data that would be related to the project?

Organization
 
- Who will administer and guide the members?
- How well can the members in the team cope with each other?
- How do the team manage to get the resources from the external organization?

Technology
 
- What softwares or hardwares are needed?
- How much will the needed software and hardware costs?
- How dooes the team manage to get hold of data and information for the project?

###Conclusion###

The organization/ team has to follow the lecturer’s instructions and learn from the lecturer, furthermore having the responsibility and work on the tasks that was assigned by the leader and team mates. 



 
