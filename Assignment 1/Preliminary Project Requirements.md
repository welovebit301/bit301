﻿### 6.0 Preliminary Project Requirements
The main features of the Educational Wiki Project Management include the following: 

1.	The research topic on the “History of Java” uploaded on the Wiki site are ensure to be accurate and original of information. Because it is a learning material that help students who enrol in the IT programme to learn Java. 
2.	The images must be relevant with the topic and original which cannot be downloaded or find through the Internet thus, it must be designed and created by the team. 
3.	The video would be the same as the images requirement which is relevant with the topic and original. Moreover, the video length must be between 5 to 10 minutes. The audio and subtitles in the video are ensure to be on the same track. When the video is created, it will be uploaded onto video sharing site such as YouTube and Viki.
4.	A “Report” feature is able to help students learn better because students are able to report to the admin if errors are found in the explanation on the learning materials. 
