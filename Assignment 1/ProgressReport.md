##Status/Progress Report

**Project Name:** WeLoveBIT301
 
**Date:** 30/09/2015

**Reporting Period:** 24/Aug/15 - 2/Oct/15

---
 
####Work completed this reporting period:
* Did the research for the project topic, History of Java. 
* Analyzed the project three sphere model and stakeholders.
* Completed all of the project  tasks for (Assignment 1) on time.


####Work to complete next reporting period:
* Continue for the project task, create a storyboard for a video and designing the images
* Prepare a PowerPoint slide to present our project to project sponsor 
* Write a lessons learned report to evaluate the success or failure of the project


####What’s going well and why:
The project topic which is in the wiki site was created properly. The work item list was clear enough to assign tasks for a team. 

####What’s not going well and why:
The learning of BitBucket, because the team would spend a lot of time to learn how to pull and push the files through the Command prompt/ Terminal. 

####Project changes:
We are able to submit our project (Assignment 1) on time, but it seems like we’ll need more time to compile our files and documents.  

####Strategies for meeting project objectives:
* Work-well in collaboration tasks, when a teammate has any problem the others will always help out the problem.
* When facing problem which we don’t comprehend, we will try to search online or ask the lecturer to give a clear explanation to solve that problem. 
* We have a routine meeting once a week on Monday to discuss about the progress of the project.

