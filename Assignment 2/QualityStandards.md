## List of Quality Standards for (History of Java Research)


Scope Statement Characteristics & Requirements

1.Links: All the links to the external sites appropriate to the project will be attached to the wiki. The reviewers can click and view the related links to get further information e.g. timeline of Java, producers and teams that created Java.

2.Image: The images that are produced by the team will be posted on the wiki to create a better and entertaining platform for the users. They will be made attractive and informative showing users the information that relates to the history of Java e.g. the producers, Java icons etc.

3.Video: The video with audio and too subtitles will be attached to the wiki for the reviewers to gain additional knowledge. It will be both fun and informative, serving as a summarized version for the written wiki and shows the evolution of Java and the deeds of Java.


## Quality Standards

|No   |Success Criteria/Requirement   |Quality Standard   |Metric   |Quality Assurance   |Quality Control   |Evaluation   |
 |---|---|---|---|---|---|---|
<<<<<<< 
 |1.  |Getting people to use the Wiki page.   |More than 150 students have go through the Wiki page within one month.   |The number of students viewed the page monthly.   |Ensure that the students go through this Wiki page when they need assistance in Java.   |Results: Less than 50 students view this wiki page regarding History of Java.   |Quality standard not met: Need to make the wiki page well known to the people used internet regularly.   |
 |2.  |All the links to the external sites appropriate to the project will be attached to the wiki.   |All the students who go through the Wiki page use at least three links that are posted on the Wiki.   |The number of links that used by the students.   |Ensure that the links correctly used by the students.   |Results: Mostly only one link is used by the students.   |Quality standard not met: Need to post more effective links to the wiki page.   | 
 |3.  |The image that produced by the team will be posted on the wiki.   |80 of the students will view the images which are attached to the Wiki.   |The number of students viewed the images posted on the wiki page.   |Ensure that the students gets entertain by the images.   |Results: More than 80% the students do view the image posted on wiki.   |Quality standard met.   |
 |4.  |The video with audio and too subtitles will be attached to the wiki.   |50% of the students will be watching the video posted on this Wiki page.   |The number of seen of the video.   |Ensure that students gain extra information by watching the video.   |Results: 60% of the students watch the video.   |Quality standard met.   |                                                                    				                          
=======

 