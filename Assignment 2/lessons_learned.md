#Lessons Learned Report

**Prepared by:** ONG MING YEE

**Date:** 10/10/2015

**Project Name:** History of Java Research
	
**Project Sponsor:** NG SHU MIN
	
**Project Manager:** ONG MING YEE
	
**Project Dates:** 1/9/2015
	
**Final Budget:** RM 34,062

		
---
**Did the project meet scope, time, cost and quality goals?**

Scope 

- The scope is met whereby the risk of failure and the chance of success analysed in the initial phase. 

- By doing the resource requirement, cost estimation and Gantt Chart they reduce the probability that the team would fail to achive the scope. 

Time

- Though there is a time constraint for the project, but the team manages to finish all the assigned tasks on time.

- The team were able to complete it on time, because we are following the Gantt Chart as much as possible.

Cost

- The cost involve in the project are the cost of the hardware and software used for the whole project.

- This is consider a failur because we were unable to complete the project without exceeding the cost needed $35,000 where the used cost be $42,719.

Quality

- The team had analysed and research as much as possible for each tasks.

- But the quality or requirement of the project would most likely depend on the customer or the supervisor of the project.

- Therefore it would consider as partially achievable for the project.

**What was the success criteria listed in the project scope statement?**

a. Complete this whole project within 14 weeks, using not more than the cost of RM100,000 and the exact cost used is $42,719. Though this criteria is a success but the estimated cost at first was $35,000. 

b.  Work hard on the project with full confidence and assure that it will be very helpful for IT students taking the Java subject. 

It does not matter that it exceeds a little longer to complete certain tasks or even cost a little more than planned, but the team will ensure that the success of this project is more important. 

c. Most importantly, is to follow the instructions and do the research according to the requirements. Using all the software and hardware such as the Bitbucket and GanttProject for online wiki creation, discussions and Gantt creation.

**Reflect on whether or not you met the project success criteria**

The project success criteria scope targets on the cost, quality, and efficiency of the project. 

These criteria are mostly achievable since the cost of the project are only the hardware and software used to complete the tasks, which are the devices used (developers' computers) and free online sources software (Bitbucket and GanttProject). 

As for the quality, the team has work in a cooperative way and analysed all available tasks, but the shortcoming of the quality comes when there are misunderstanding of the requirements of certain tasks in the project. 

Lastly for the efficiency of the project would be achievable as each member were able to learn and utilise the required software or platform for the wiki creation, Gantt charts, images and video. 

**In terms of managing the project, what were the main lessons your team learned?**

In terms of managing, the team actually learns that the time management is the most important thing in the project. 

Time management for all tasks and work assigned for each members has to be accurate and finished on the due date, so other tasks are not delayed or have lesser time to work on. 

Next would be the knowledge needed, members have to search, learn and pay attention to the supervisor to gain knowledge and the requirements for the project, such as the Bit bucket, Gantt Project and Microsoft Excel tutorials.

Lastly, would be the team spirit. The communication between members are important, therefore we have weekly meetings together to discuss the progress of the project and the fidelity of each tasks.

**Describe one example of what went right on this project.**

The communication between all the members in the group is one of things that went right during the development of the project, where all members understand each other’s weaknesses and strengths, so tasks assigning would be easier and fair.

Next would be the management of time and resources for the project. The team were able to utilize all the software (Bitbucket, GanttProject, Microsot Excel, GIT and Markdown). This is succesfully done by paying full attention to the supervisor and also research on various websites.


**Describe one example of what went wrong on this project.**

The thing that went wrong for the project is the Gantt Chart that doesn’t match with the real time working for the team, because when the Gantt Chart was produced, all the tasks written are assumptions from the team. 

The criteria of completing the project in less than $100,000 is successful but the estimated cost was $35,000 but the actual cost used is $42,719, this has led to inaccuracy for the financing part of the project.

When it comes to reality where the works are distributed and started, the timeline and hours it takes to work on doesn’t match the work items list timeline, and the time estimated to complete the tasks.

There are also issues found when the team is using the softwares such as creating a table in markdown file and also using the GIT command. Fortunately, the issues are shared and discussed in the Bitbucket. 

**What will you do differently on the next project based on your experience working on this project?**

The team would suggest to work on the project as soon as possible when the project is given. 

Since initialising the project would spend a lot of time, such as doing research and analyse on the topic and understanding the topic. In addition, the team would have to recognize the utility needed and most of the requirements of the project. 

It would be best to not delay any tasks assigned and reduce work conflict for future tasks, because there are tasks that cannot be started before certain tasks are completed.

**Lesson Learn Image Here**



E.g. Task C cannot commence without having Task A and B being completed as same goes for Task D.
Therefore the predecessor of certain tasks are to be completed as soon as possible without slacking off.